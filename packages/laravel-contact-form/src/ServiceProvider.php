<?php

namespace AlexVargasDev\ContactForm;

use Illuminate\Support\ServiceProvider as Base;

class ServiceProvider extends Base{
    
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        
    }}